var check = 0;
var state = 0;
var position = 0;
var zeichen = "";
var schritt = 0;
var richtig = ["BTBPTVVETE", "BTBPTTTTVVETE", "BTBTSSSXXVVETE", "BPBPTTTTVVEPE", "BPBPVVEPE", "BPBTSXSEPE", "BPBTXXTTTVPXTVVEPE"];
var falsch = ["BTBPVSETE", "BTBXTVVETE", "BPBTXEEPE", "BTSPVVETE", "BTBPTVXETE", "BPBPEPXVPSEPE", "BPBTVPSEPE", "BTBTSSXXVPXVTVVXTE"];

function setup() {
  createCanvas(1000, 600);
  //checkValidity("BPVVE");
  title = createElement('h1', 'Abgabe 4');
  title.position(50, 0);

  text('Anleitung:\nRichtig erzeugen: Erzeugt einen korrekten Ausdruck\nFalsch erzeugen: Erzeugt einen falschen Ausdruck\nAusführen: Führt die Prüfung mit Visualisierungen aus.', 700, 50);
  
  anleitung = createElement('h4', '');
  anleitung.position(500, 0);

  greeting = createElement('h3', 'Zeichenkette:');
  greeting.position(50, 50);

  input = createInput();
  input.position(greeting.x + 120, greeting.y + 20);

  buttonRight = createButton('Richtig erzeugen');
  buttonRight.position(input.x + input.width + 5, input.y);
  buttonRight.mousePressed(richtigErzeugen);

  buttonFalse = createButton('Falsch erzeugen');
  buttonFalse.position(buttonRight.x + buttonRight.width + 5, buttonRight.y);
  buttonFalse.mousePressed(falschErzeugen);

  buttonNext = createButton('Ausführen');
  buttonNext.position(buttonFalse.x + buttonFalse.width + 5, buttonFalse.y);
  buttonNext.mousePressed(next);
}

function draw() {
  if(check == 0){
    stroke(100);
    textSize(20);
    fill(255, 255, 255);

    //#region Band
    // Band
    line(greeting.x, greeting.y + 60, greeting.x + 600, greeting.y + 60);
    line(greeting.x, greeting.y + 90, greeting.x + 600, greeting.y + 90);
    line(greeting.x, greeting.y + 60, greeting.x, greeting.y + 90);

    line(greeting.x + 30, greeting.y + 60, greeting.x + 30, greeting.y + 90);
    line(greeting.x + 60, greeting.y + 60, greeting.x + 60, greeting.y + 90);
    line(greeting.x + 90, greeting.y + 60, greeting.x + 90, greeting.y + 90);
    line(greeting.x + 120, greeting.y + 60, greeting.x + 120, greeting.y + 90);
    line(greeting.x + 150, greeting.y + 60, greeting.x + 150, greeting.y + 90);
    line(greeting.x + 180, greeting.y + 60, greeting.x + 180, greeting.y + 90);
    line(greeting.x + 210, greeting.y + 60, greeting.x + 210, greeting.y + 90);
    line(greeting.x + 240, greeting.y + 60, greeting.x + 240, greeting.y + 90);
    line(greeting.x + 270, greeting.y + 60, greeting.x + 270, greeting.y + 90);
    line(greeting.x + 300, greeting.y + 60, greeting.x + 300, greeting.y + 90);
    line(greeting.x + 330, greeting.y + 60, greeting.x + 330, greeting.y + 90);
    line(greeting.x + 360, greeting.y + 60, greeting.x + 360, greeting.y + 90);
    line(greeting.x + 390, greeting.y + 60, greeting.x + 390, greeting.y + 90);
    line(greeting.x + 420, greeting.y + 60, greeting.x + 420, greeting.y + 90);
    line(greeting.x + 450, greeting.y + 60, greeting.x + 450, greeting.y + 90);
    line(greeting.x + 480, greeting.y + 60, greeting.x + 480, greeting.y + 90);
    line(greeting.x + 510, greeting.y + 60, greeting.x + 510, greeting.y + 90);
    line(greeting.x + 540, greeting.y + 60, greeting.x + 540, greeting.y + 90);
    line(greeting.x + 570, greeting.y + 60, greeting.x + 570, greeting.y + 90);
    //#endregion Band

    // Kopf
    kopfPos(0);

    //#region Alt

    /*// Zustände
    ellipse(100, 200, 50, 50);  // Zustand 1
    ellipse(300, 100, 50, 50);  // Zustand 2
    ellipse(300, 300, 50, 50);  // Zustand 3
    ellipse(500, 100, 50, 50);  // Zustand 4
    ellipse(500, 300, 50, 50);  // Zustand 5
    ellipse(700, 200, 50, 50);  // Zustand 6

    // Zustandsübergänge
    fill(0, 0, 0);
    line(10, 200, 75, 200);     // Anfangszustand --> 1
    text("B", 30, 185);
    line(125, 200, 275, 100);   // 1 --> 2
    text("T", 180, 135);
    line(125, 200, 275, 300);   // 1 --> 3
    text("P", 180, 285);
    fill(255, 255, 255);
    arc(300, 65, 35, 35, 1.5*PI / 2, 4.5 * PI / 2);   // 2 --> 2
    arc(300, 335, 35, 35, 3.5*PI / 2, 2.5 * PI / 2);  // 3 --> 3
    fill(0, 0, 0);
    text("S", 293, 30);
    text("T", 293, 380);
    line(325, 100, 475, 100);   // 2 --> 4
    text("X", 390, 80);
    line(320, 285, 480, 115);   // 4 --> 3
    text("X", 390, 180);
    line(325, 300, 475, 300);   // 3 --> 5
    text("V", 390, 330);
    line(500, 125, 500, 275);   // 5 --> 4
    text("P", 520, 200);
    line(525, 100, 675, 200);   // 4 --> 6
    text("S", 600, 135);
    line(525, 300, 675, 200);   // 5 --> 6
    text("V", 600, 285);
    line(725, 200, 790, 200);   // 6 --> Endzustand
    text("E", 745, 185);

    // Pfeile
    push();
    var angle = atan2(115 - 285, 480 - 320); //gets the angle of the line
    translate(320, 285); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(200 - 200, 10 - 75); //gets the angle of the line
    translate(75, 200); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(200 - 100, 125 - 275); //gets the angle of the line
    translate(275, 100); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(200 - 300, 125 - 275); //gets the angle of the line
    translate(275, 300); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(100 - 100, 325 - 475); //gets the angle of the line
    translate(475, 300); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(100 - 100, 325 - 475); //gets the angle of the line
    translate(475, 100); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(100 - 200, 525 - 675); //gets the angle of the line
    translate(675, 200); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(300 - 200, 525 - 675); //gets the angle of the line
    translate(675, 200); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    line(500, 125, 500, 275);
    push();
    var angle = atan2(275 - 125, 500 - 500); //gets the angle of the line
    translate(500, 125); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(4.5 * PI / 2, 1.5 * PI / 2); //gets the angle of the line
    translate(312, 325); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(-4.5 * PI / 2, 1.5 * PI / 2); //gets the angle of the line
    translate(312, 75); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    push();
    var angle = atan2(200 - 200, 725 - 790); //gets the angle of the line
    translate(790, 200); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.5, 15, 15*0.5, 15, 0, -15/2); 
    pop();

    checkValidity(input.value());*/
    //#endregion Alt
  
    automatErstellen();
    pfeileErstellen();
    beschriftungenErstellen();
  }
  check = 1;
}
//#region 
async function checkValidity(input){
  fill(255, 255, 255);
  rect(90, 424, 700, 40);
  fill(0, 0, 0);
  await sleep(1000);
  if(input[0] == "B"){
    markState(1);
    state1(input, 1);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
async function state1(input, pos){
  await sleep(1000);
  if(input[pos] == "T"){
    markState(2);
    state2(input, pos+1);
  } else if(input[pos] == "P"){
    markState(3);
    state3(input, pos+1);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
async function state2(input, pos){
  await sleep(1000);
  if(input[pos] == "S"){
    markState(2);
    state2(input, pos+1);
  } else if(input[pos] == "X"){
    markState(4);
    state4(input, pos+1);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
async function state3(input, pos){
  await sleep(1000);
  if(input[pos] == "T"){
    markState(3);
    state3(input, pos+1);
  } else if(input[pos] == "V"){
    markState(5);
    state5(input, pos+1);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
async function state4(input, pos){
  await sleep(1000);
  if(input[pos] == "X"){
    markState(3);
    state3(input, pos+1);
  } else if(input[pos] == "S"){
    markState(6);
    state6(input, pos+1);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
async function state5(input, pos){
  await sleep(1000);
  if(input[pos] == "P"){
    markState(4);
    state4(input, pos+1);
  } else if(input[pos] == "V"){
    markState(6);
    state6(input, pos+1);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
async function state6(input, pos){
  await sleep(1000);
  if(input[pos] == "E"){
    markState(-1);
    fill(0, 0, 0);
    text("Die Zeichenkette " + input + " ist gültig!", 100, 450);
  } else text("Die Zeichenkette " + input + " ist ungültig!", 100, 450);
}
function markState(state){
  // Setze alle Zustände zurück
  fill(255, 255, 255);
  ellipse(100, 200, 50, 50);  // Zustand 1
  ellipse(300, 100, 50, 50);  // Zustand 2
  ellipse(300, 300, 50, 50);  // Zustand 3
  ellipse(500, 100, 50, 50);  // Zustand 4
  ellipse(500, 300, 50, 50);  // Zustand 5
  ellipse(700, 200, 50, 50);  // Zustand 6

  if(state == 1){
    fill(0,0,0);
    ellipse(100, 200, 50, 50);  // Zustand 1
  } else if(state == 2){
    fill(0,0,0);
    ellipse(300, 100, 50, 50);  // Zustand 2
  } else if(state == 3){
    fill(0,0,0);
    ellipse(300, 300, 50, 50);  // Zustand 3
  } else if(state == 4){
    fill(0,0,0);
    ellipse(500, 100, 50, 50);  // Zustand 4
  } else if(state == 5){
    fill(0,0,0);
    ellipse(500, 300, 50, 50);  // Zustand 5
  } else if(state == 6){
    fill(0,0,0);
    ellipse(700, 200, 50, 50);  // Zustand 6
  }
}
function senden(){
  check = 0;
}
function next(){
  if(state == 0 && input.value()[position] == "B"){
    state = 1;
    markState(1);
    position++;
  } else if(state == 1 && input.value()[position] == "T"){
    state = 2;
    markState(2);
    position++;
  } else if(state == 1 && input.value()[position] == "P"){
    state = 3;
    markState(3);
    position++;
  } else if(state == 2 && input.value()[position] == "S"){
    state = 2;
    markState(2);
    position++;
  } else if(state == 2 && input.value()[position] == "X"){
    state = 4;
    markState(4);
    position++;
  } else if(state == 3 && input.value()[position] == "T"){
    state = 3;
    markState(3);
    position++;
  } else if(state == 3 && input.value()[position] == "V"){
    state = 5;
    markState(5);
    position++;
  } else if(state == 4 && input.value()[position] == "x"){
    state = 3;
    markState(3);
    position++;
  } else if(state == 4 && input.value()[position] == "S"){
    state = 6;
    markState(6);
    position++;
  } else if(state == 5 && input.value()[position] == "P"){
    state = 4;
    markState(4);
    position++;
  } else if(state == 5 && input.value()[position] == "V"){
    state = 6;
    markState(6);
    position++;
  } else if(state == 6 && input.value()[position] == "E"){
    state = 7;
    markState(7);
    text("Die Zeichenkette " + input.value() + " ist gültig!", 100, 450);
    position++;
  } else{
    text("Die Zeichenkette " + input.value() + " ist ungültig!", 100, 450);
  }
}
//#endregion

function richtigErzeugen(){
  var rand = Math.floor(Math.random()*richtig.length);
  input.value(richtig[rand]);      
}

function falschErzeugen(){
  var rand = Math.floor(Math.random()*falsch.length);
  input.value(falsch[rand]);  
}

function next(){
  bandStart();
  nodeQ0(0);
}

//#region Nodes
async function nodeQ0(pos){
  markQ(0, true);
  await sleep(500);
  if(zeichen[pos] == 'B'){
    zeichen = zeichen.replace('B', '⌴');
    bandZeichen(pos);
    nodeQ1(pos + 1);
    text(zeichen1, 200, 30);
  } 
  else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(0, false);
  }
}
async function nodeQ1(pos){
  markQ(1, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == 'T'){
    zeichen = zeichen.replace('T', '$');
    bandZeichen(pos);
    nodeQ2(pos + 1);
  } 
  else if(zeichen[pos] == 'P'){
    zeichen = zeichen.replace('P', '%');
    bandZeichen(pos);
    nodeQ2(pos + 1);
  }
  else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(1, false);
  }
}
async function nodeQ2(pos){
  markQ(2, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == 'B') {
    zeichen = zeichen.replace('B', '⌴');
    bandZeichen(pos);
    nodeQ3(pos + 1);
  } else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(2, false);
  }
}
async function nodeQ3(pos){
  markQ(3, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == 'T') {
    zeichen = zeichen.replace('T', '⌴');
    bandZeichen(pos);
    nodeQ4(pos + 1);
  } else if(zeichen[pos] == 'P') {
    zeichen = zeichen.replace('P', '⌴');
    bandZeichen(pos);
    nodeQ5(pos + 1);
  } else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(3, false);
  }
}
async function nodeQ4(pos){
  markQ(4, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == 'S'){
    zeichen = zeichen.replace('S', '⌴');
    bandZeichen(pos);
    nodeQ4(pos + 1);
  }
  else if(zeichen[pos] == 'X') {
    zeichen = zeichen.replace('X', '⌴');
    bandZeichen(pos);
    nodeQ6(pos + 1);
  } else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(4, false);
  }
}
async function nodeQ5(pos){
  markQ(5, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == 'T') {
    zeichen = zeichen.replace('T', '⌴');
    bandZeichen(pos);
    nodeQ5(pos + 1);
  }
  else if(zeichen[pos] == 'V'){
    zeichen = zeichen.replace('V', '⌴');
    bandZeichen(pos);
    nodeQ7(pos + 1);
  }
  else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(5, false);
  }
}
async function nodeQ6(pos){
  markQ(6, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == 'S') {
    zeichen = zeichen.replace('S', '⌴');
    bandZeichen(pos);
    nodeQ8(pos + 1);
  }
  else if(zeichen[pos] == 'X') {
    zeichen = zeichen.replace('X', '⌴');
    bandZeichen(pos);
    nodeQ5(pos + 1);
  }
  else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(6, false);
  }
}
async function nodeQ7(pos){
  markQ(7, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == 'P') {
    zeichen = zeichen.replace('P', '⌴');
    bandZeichen(pos);
    nodeQ6(pos + 1);
  }
  else if(zeichen[pos] == 'V'){
    zeichen = zeichen.replace('V', '⌴');
    bandZeichen(pos);
    nodeQ8(pos + 1);
  }
  else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(7, false);
  }
}
async function nodeQ8(pos){
  markQ(8, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == 'E') {
    zeichen = zeichen.replace('E', '⌴');
    bandZeichen(pos);
    nodeQ9(pos + 1);
  }
  else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(8, false);
  }
}
async function nodeQ9(pos){
  markQ(9, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == 'T') {
    zeichen = zeichen.replace('T', '⌴');
    bandZeichen(pos);
    nodeQ10(pos + 1);
  }
  else if(zeichen[pos] == 'P') {
    zeichen = zeichen.replace('P', '⌴');
    bandZeichen(pos);
    nodeQ11(pos + 1);
  }
  else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(9, false);
  }
}
async function nodeQ10(pos){
  markQ(10, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == 'E') {
    zeichen = zeichen.replace('E', '⌴');
    bandZeichen(pos);
    nodeQ12(pos + 1);
  }
  else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(10, false);
  }
}
async function nodeQ11(pos){
  markQ(11, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == 'E'){
    zeichen = zeichen.replace('E', '⌴');
    bandZeichen(pos);
    nodeQ13(pos + 1);
  }
  else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(11, false);
  }
}
async function nodeQ12(pos){
  markQ(12, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == '⌴') {
    zeichen = zeichen.replace('⌴', '⌴');
    bandZeichen(pos);
    nodeQ12(pos - 1);
  }
  else if(zeichen[pos] == '$') {
    zeichen = zeichen.replace('$', '⌴');
    bandZeichen(pos);
    nodeQ14(pos + 1);
  }
  else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(12, false);
  }
}
async function nodeQ13(pos){
  markQ(13, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == '⌴') {
    zeichen = zeichen.replace('⌴', '⌴');
    bandZeichen(pos);
    nodeQ13(pos - 1);
  }
  else if(zeichen[pos] == '%') {
    zeichen = zeichen.replace('%', '⌴');
    bandZeichen(pos);
    nodeQ14(pos + 1);
  }
  else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(13, false);
  }
}
async function nodeQ14(pos){
  markQ(14, true);
  await sleep(500);
  kopfPos(pos);
  if(zeichen[pos] == '⌴') {
    zeichen = zeichen.replace('⌴', '⌴');
    fill(20, 220, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    text(zeichen[pos], 58 + (pos * 30), 132);
    markQ(15, true);
  }
  else{
    fill(220, 20, 20);
    rect(50 + (pos * 30), greeting.y + 60, 30, 30);
    fill(255);
    markQ(14, false);
  }
}
//#endregion Nodes

function kopfPos(pos){
  // Kopf
  stroke(255);
  fill(255);
  rect(50, 141, 600, 90);

  stroke(0);
  fill(0);
  line(50 + (pos * 30), 200, 80 + (pos * 30), 200);
  line(50 + (pos * 30), 230, 80 + (pos * 30), 230);
  line(50 + (pos * 30), 200, 50 + (pos * 30), 230);
  line(80 + (pos * 30), 200, 80 + (pos * 30), 230);

  line(65 + (pos * 30), 200, 65 + (pos * 30), 148);
  
  push();
  var angle = atan2(90, 0); //gets the angle of the line
  translate(65 + (pos * 30), 152); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 7, 5, 7, 0, -15/2); 
  pop();
  //#endregion Kopf
}

function bandZeichen(pos){
  textSize(20);
  stroke(0);
  fill(255);
  rect(50 + (pos * 30), greeting.y + 60, 30, 30);

  stroke(255);
  fill(0);
  text(zeichen[pos], 58 + (pos * 30), 132);
}

function bandStart(){
  zeichen = input.value();
  kopfPos(0);
  for(var i = zeichen.length; zeichen.length < 19; i++){
    zeichen = zeichen + "⌴";
  }
  textSize(20);
  for(var i = 0; i < zeichen.length; i++){
    bandZeichen(i);
  }
  for(var i = zeichen.length; i < 19; i++){
    stroke(0);
    fill(255);
    rect(50 + (i * 30), greeting.y + 60, 30, 30);
  }
}

function automatErstellen(){
  resetAutomat();

  fill(255);
  stroke(0);

  line(50, 400, 85, 400);
  ellipse(100, 400, 30, 30);
  ellipse(175, 400, 30, 30);
  ellipse(250, 400, 30, 30);
  ellipse(325, 400, 30, 30);
  line(115, 400, 160, 400);
  line(190, 400, 235, 400);
  line(265, 400, 310, 400);

  ellipse(400, 350, 30, 30);
  ellipse(400, 450, 30, 30);
  line(337, 390, 385, 350);
  line(337, 410, 385, 450);

  ellipse(475, 350, 30, 30);
  ellipse(475, 450, 30, 30);
  arc(400, 328, 25, 40, 1.5*PI / 2, 4.5 * PI / 2);
  arc(400, 472, 25, 40, -4.5 * PI / 2, -1.5*PI / 2);
  line(415, 350, 460, 350);
  line(415, 450, 460, 450);
  line(465, 362, 410, 438);
  line(475, 365, 475, 435);

  ellipse(550, 400, 30, 30);
  ellipse(625, 400, 30, 30);
  line(490, 450, 538, 410);
  line(490, 350, 538, 390);
  line(565, 400, 610, 400);

  ellipse(700, 350, 30, 30);
  ellipse(700, 450, 30, 30);
  line(637, 390, 685, 350);
  line(637, 410, 685, 450);

  ellipse(775, 350, 30, 30);
  ellipse(775, 450, 30, 30);
  line(715, 350, 760, 350);
  line(715, 450, 760, 450);

  ellipse(850, 400, 30, 30);
  ellipse(925, 400, 30, 30);
  ellipse(925, 400, 27, 27);
  arc(775, 328, 25, 40, 1.5*PI / 2, 4.5 * PI / 2);
  arc(775, 472, 25, 40, -4.5 * PI / 2, -1.5*PI / 2);
  line(790, 450, 838, 410);
  line(790, 350, 838, 390);
  line(865, 400, 910, 400);

  pfeileErstellen();
}

function pfeileErstellen(){
  stroke(0);
  fill(0);

  push();
  var angle = atan2(0, -90); //gets the angle of the line
  translate(80, 400); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(0, -90); //gets the angle of the line
  translate(155, 400); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(0, -90); //gets the angle of the line
  translate(230, 400); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(0, -90); //gets the angle of the line
  translate(305, 400); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();



  push();
  var angle = atan2(45, -45); //gets the angle of the line
  translate(380, 355); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(-45, -45); //gets the angle of the line
  translate(380, 445); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();



  push();
  var angle = atan2(0, -90); //gets the angle of the line
  translate(455, 350); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(-90, 15); //gets the angle of the line
  translate(411, 333); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(-55, 45); //gets the angle of the line
  translate(412, 435); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(0, -90); //gets the angle of the line
  translate(455, 450); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(90, 15); //gets the angle of the line
  translate(411, 467); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(90, 0); //gets the angle of the line
  translate(475, 370); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();



  push();
  var angle = atan2(-45, -45); //gets the angle of the line
  translate(535, 390); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(45, -45); //gets the angle of the line
  translate(535, 410); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();


  
  push();
  var angle = atan2(0, -90); //gets the angle of the line
  translate(605, 400); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();


  
  push();
  var angle = atan2(45, -45); //gets the angle of the line
  translate(680, 355); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(-45, -45); //gets the angle of the line
  translate(680, 445); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();



  push();
  var angle = atan2(0, -90); //gets the angle of the line
  translate(755, 350); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(-90, 15); //gets the angle of the line
  translate(786, 333); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(0, -90); //gets the angle of the line
  translate(755, 450); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();
  
  push();
  var angle = atan2(90, 15); //gets the angle of the line
  translate(786, 467); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();



  push();
  var angle = atan2(-45, -45); //gets the angle of the line
  translate(835, 390); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  push();
  var angle = atan2(45, -45); //gets the angle of the line
  translate(835, 410); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();

  

  push();
  var angle = atan2(0, -90); //gets the angle of the line
  translate(905, 400); //translates to the destination vertex
  rotate(angle-HALF_PI); //rotates the arrow point
  triangle(-5, 10, 5, 10, 0, -5); 
  pop();
}

function beschriftungenErstellen(){
  fill(0);
  stroke(255);
  textSize(12);

  text("q0", 93, 403);
  text("q1", 168, 403);
  text("q2", 243, 403);
  text("q3", 318, 403);
  text("q4", 393, 353);
  text("q5", 393, 453);
  text("q6", 468, 353);
  text("q7", 468, 453);
  text("q8", 543, 403);
  text("q9", 618, 403);
  text("q10", 690, 353);
  text("q11", 690, 453);
  text("q12", 765, 353);
  text("q13", 765, 453);
  text("q14", 840, 403);
  text("q15", 915, 403);

  
  text("B/⌴, R", 115, 390);
  
  text("T/$, R", 190, 375);
  text("P/%, R", 190, 390);
  
  text("B/⌴, R", 265, 390);
  
  text("T/⌴, R", 320, 370);
  text("P/⌴, R", 320, 440);

  text("S/⌴, R", 385, 290);
  text("X/⌴, R", 420, 340);

  text("T/⌴, R", 385, 510);
  text("V/⌴, R", 420, 470);

  text("X/⌴, R", 390, 400);
  
  text("S,⌴, R", 520, 370);
  
  text("P/⌴, R", 480, 400);
  text("V/⌴, R", 520, 440);

  text("E/⌴, R", 570, 390);
  
  text("T/⌴, R", 620, 370);
  text("P/⌴, R", 620, 440);
  
  text("E/⌴, R", 720, 340);
  
  text("E/⌴, R", 720, 470);
  
  text("$/⌴, R", 820, 370);
  text("%/⌴, R", 820, 440);

  text("⌴/⌴, L", 760, 290);

  text("⌴/⌴, L", 760, 510);

  text("⌴/⌴, R", 870, 390);

  legende();

  textSize(20);
}

function resetAutomat(){
  fill(255);
  stroke(0);
  rect(50, 240, 930, 310);
}

function sleep(millisecondsDuration)
{
  return new Promise((resolve) => {
    setTimeout(resolve, millisecondsDuration);
  })
}

function markQ(qNo, rf){
  resetAutomat();
  automatErstellen();
  if(rf == true){
    fill(20, 220, 20);
  }
  else{
    fill(220, 20, 20);
  }
  stroke(0);
  if(qNo == 0){
    ellipse(100, 400, 30, 30);
  } else if(qNo == 1){
    ellipse(175, 400, 30, 30);
  } else if(qNo == 2){
    ellipse(250, 400, 30, 30);
  } else if(qNo == 3){
    ellipse(325, 400, 30, 30);
  } else if(qNo == 4){
    ellipse(400, 350, 30, 30);
  } else if(qNo == 5){
    ellipse(400, 450, 30, 30);
  } else if(qNo == 6){
    ellipse(475, 350, 30, 30);
  } else if(qNo == 7){
    ellipse(475, 450, 30, 30);
  } else if(qNo == 8){
    ellipse(550, 400, 30, 30);
  } else if(qNo == 9){
    ellipse(625, 400, 30, 30);
  } else if(qNo == 10){
    ellipse(700, 350, 30, 30);
  } else if(qNo == 11){
    ellipse(700, 450, 30, 30);
  } else if(qNo == 12){
    ellipse(775, 350, 30, 30);
  } else if(qNo == 13){
    ellipse(775, 450, 30, 30);
  } else if(qNo == 14){
    ellipse(850, 400, 30, 30);
  } else if(qNo == 15){
    ellipse(925, 400, 27, 27);
  }
  beschriftungenErstellen();
}

function legende(){
  textSize(10);
  stroke(255);
  fill(0);
  text("T = (Q, Σ, Γ, δ, q0, qaccept, qreject)", 60, 255);
  text("Q = {q0, q1, ..., q15}", 60, 270);
  text("∑ = {B, T, S, V, X, P, E}", 60, 285);
  text("Γ = {B, T, S, V, X, P, E, ⌴, $, %}", 60, 300);
  text("δ = Q × Γ → Q × Γ × {L,R}", 60, 315);
  text("q0 = {q0}", 60, 330);
  text("qaccept = {q15}", 60, 345);
}